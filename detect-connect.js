function $$httpAPI(method, path, body) {
  return new Promise(resolve => {
    $httpAPI(method, path, body, resolve)
  })
}

(async function () {
  if (!$response.body.includes('Access Denied')) {
    $done({})
    return
  }

  await $$httpAPI('POST', '/v1/profiles/reload')
  $notification.post('賽馬娘連線錯誤！', '', '重整完成，請點遊戲畫面重試')

  $done({})
})()
